# Playground

* Import Java S2I image

```
oc import-image my-redhat-openjdk-18/openjdk18-openshift --from=registry.access.redhat.com/redhat-openjdk-18/openjdk18-openshift --confirm
```

* Create `ConfigMap` from file

```
oc create configmap sample \
    --from-file=sample.sh
```
